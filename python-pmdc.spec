%define srcname pmdc
%define version 1.1.0
%define release 1

Name: python-%{srcname}
Version: %{version}
Release: %{release}%{?dist}
Summary: LIGO Poor Man's DiskCache
License: GPLv3+
Url: https://git.ligo.org/computing/gwdatafind/pmdc
Vendor: Duncan Macleod <duncan.macleod@ligo.org>

BuildArch: noarch
Prefix: %{_prefix}

Source0: https://software.igwn.org/sources/source/%{name}-%{version}.tar.gz

# build
BuildRequires: python-rpm-macros
BuildRequires: python3
BuildRequires: python3-rpm-macros
BuildRequires: python3-setuptools

%description
Efficiently search file systems for frames, cache results in various formats.

# -- python3-pmdc

%package -n python%{python3_pkgversion}-%{srcname}
Summary: Python %{python3_version} PMDC library
Requires: python%{python3_pkgversion}
%{?python_provide:%python_provide python%{python3_pkgversion}-%{srcname}}
%description -n python%{python3_pkgversion}-%{srcname}
Efficiently search file systems for frames, cache results in various formats.
This package provides the Python %{python3_version} library.

%package -n %{srcname}
Summary: %{summary}
Requires: python%{python3_pkgversion}-%{srcname} = %{version}-%{release}
Conflicts: python-pmdc < 1.0.3-1
%description -n %{srcname}
Efficiently search file systems for frames, cache results in various formats.
This package provides the command-line interfaces

# -- build steps

%prep
%setup -q -n %{name}-%{version}

%build
%py3_build

%install
%py3_install "--install-scripts=/usr/sbin"

# -- files

%files -n python%{python3_pkgversion}-%{srcname}
%doc README.rst
%license LICENSE
%{python3_sitelib}/*

%files -n %{srcname}
%doc README.rst
%license LICENSE
%{_sbindir}/*
%{_mandir}/*/*

# -- changelog

%changelog
* Thu Apr 26 2022 Duncan Macleod <duncan.macleod@ligo.org> - 1.1.0-1
- update to 1.1.0
- move to Python 3 only
- use wheels to build/install

* Mon Mar 28 2022 Duncan Macleod <duncan.macleod@ligo.org> - 1.0.3-1
- first cut at RHEL packaging
