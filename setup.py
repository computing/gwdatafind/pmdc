# -*- coding: utf-8 -*-

import re

from setuptools import setup


def find_version(path, varname="__version__"):
    """Parse the version metadata variable in the given file.
    """
    with open(path, 'r') as fobj:
        version_file = fobj.read()
    version_match = re.search(
        r"^{0} = ['\"]([^'\"]*)['\"]".format(varname),
        version_file,
        re.M,
    )
    if version_match:
        return version_match.group(1)
    raise RuntimeError("Unable to find version string.")


setup(name="python-pmdc",
      description="LIGO pmdc",
      version=find_version("pmdc.py"),
      author="Jeff Kline",
      author_email="ldr@ligo.org",
      license="GPL-3.0-or-later",
      py_modules=["pmdc"],
      setup_requires=["setuptools"],
      entry_points={
          "console_scripts": [
              "pmdc=pmdc:main",
          ],
      },
      data_files=[
          ("share/man/man8", ["man/pmdc.8"]),
      ],
      classifiers=[
          'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
          'Programming Language :: Python',
          'Programming Language :: Python :: 2.7',
          'Programming Language :: Python :: 3.6',
          'Programming Language :: Python :: 3.7',
          'Programming Language :: Python :: 3.8',
          'Programming Language :: Python :: 3.9',
          'Programming Language :: Python :: 3.10',
      ],
)
